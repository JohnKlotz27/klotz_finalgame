﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    private Transform target;

    public float HeroSpeed = 10f;
    public string enemyTag = "Enemy";
    public float range = 2f;

    private Transform herotarget;
    private int wavepointIndex = 13;

    // Start is called before the first frame update
    void Start()
    {
        herotarget = Waypoints.points[12];
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = herotarget.position - transform.position;
        transform.Translate(dir.normalized * HeroSpeed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, herotarget.position) <= 0.4f)
        {
            GetNextWaypoint();
        }

        if (target == null)
        {
            return;
        }
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex <= Waypoints.points.Length - 12)
        {
            Destroy(gameObject);
            return;
        }

        wavepointIndex--;
        herotarget = Waypoints.points[wavepointIndex];
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
                
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            Debug.Log("Found Enemy");
        }
        else
        {
            target = null;
        }
    }
}
