﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    public Transform enemyPrefab;

    public Transform spawnPoint;

    public float timebetweenWaves = 5f;
    private float countdown = 2f;
    public static float countdownGlobal;

    public Text waveCountDownText;

    private int waveIndex = 0;

    private void Start()
    {
        countdownGlobal = countdown;
    }

    private void Update()
    {
        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timebetweenWaves;
        }

        countdown -= Time.deltaTime;

        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);

        waveCountDownText.text = string.Format("{0:00.00}", countdown);
        countdownGlobal = countdown;
    }

        IEnumerator SpawnWave()
    {
        waveIndex++;
        PlayerStats.Rounds++;

        for(int i = 0; i < (waveIndex + PlayerStats.Rounds); i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(0.35f);
        } 


    }

    void SpawnEnemy()
    {
        Instantiate(enemyPrefab, spawnPoint.position, spawnPoint.rotation);
    }
    
}
