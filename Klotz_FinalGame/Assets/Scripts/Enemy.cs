﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float startSpeed = 10f;

    public float speed = 10f;

    public float EnemyHealth = 100f;

    public int worth = 50;

    private void Start()
    {
        speed = startSpeed;
        
    }

    private void Update()
    {
        
        if (WaveSpawner.countdownGlobal <= 0f)
        {
            HealthIncrease();
            SpeedIncrease();
        }
    }


    public void TakeDamage(float amount)
    {
        EnemyHealth -= amount;

        if(EnemyHealth <= 0)
        {
            Die();
        }
    }

    public void Slow (float pct)
    {
        speed = startSpeed * (1f - pct);
    }

    void Die()
    {
        PlayerStats.Money += worth;
        Destroy(gameObject);
    }

    public void HealthIncrease()
    {
        EnemyHealth = EnemyHealth += 50.0f;
    }

    public void SpeedIncrease()
    {
        startSpeed = startSpeed += 1.0f;
    }

}
